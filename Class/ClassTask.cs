using System;
using System.Collections.Generic;
using System.Linq;

namespace Class
{
    public class Rectangle {

        private double sideA;
        private double sideB;
    
        public Rectangle(double a, double b)
        {
            this.sideA = a;
            this.sideB = b;
        }
   
        public Rectangle(double a)
        {
            this.sideA = a;
            //Testing
            this.sideB = 5;
            
            
        }
    
        public Rectangle()
        {
            this.sideA = 4;
            this.sideB = 3;
        }

    
        public double GetSideA()
        {
            return sideA;
        }
    
        public double GetSideB()
        {
            return sideB;
        }
    
        public double Area()
        {
            return sideA * sideB;
        }
    
    
         public double Perimeter()
        {
            return (sideA + sideB) * 2;
        }
   
        public bool IsSquare() 
        {
            return sideA==sideB;
        }
    
        public void ReplaceSides()
        {
            double temp = sideA;
            sideA = sideB;
            sideB = temp;
        }
    }
     public class ArrayRectangles
    {
        private Rectangle[] rectangle_array;

        
        public ArrayRectangles(int n)
        {
            rectangle_array = new Rectangle[n];
        }

        
        public ArrayRectangles(Rectangle[] rectangles)
        {
            rectangle_array = rectangles;
        }

        //TODO: Define constructor that gets enumerable or array of rectangles. Constructor should assign them to its field
        public ArrayRectangles(IEnumerable<Rectangle> rectangles)
        {
            rectangle_array = rectangles.ToArray();
        }

       
        public bool AddRectangle(Rectangle rectangle)
        {
            for (int i = 0; i < rectangle_array.Length; i++)      
            {
                if (rectangle_array[i] == null)
                {
                    rectangle_array[i] = rectangle;
                    return true;
                }
            }
            return false;
        }

        //RETEST -------------------------------------------------
        
        public int NumberMaxArea()
        {
            double maxArea = 0;
            int numberOfRectangle = 0;
            for (int i = 0; i < rectangle_array.Length; i++)
            {
                //if (rectangle_array[i].Area() > maxArea)                 
                if (rectangle_array[i] != null && rectangle_array[i].Area() > maxArea)
                {
                    maxArea = rectangle_array[i].Area();
                    numberOfRectangle = i;
                }
            }
            return numberOfRectangle;
        }

       
        public int NumberMinPerimeter()
        {
            double minPerimeter = double.MaxValue;
            int numberOfRectangle = 0;
            for (int i = 0; i < rectangle_array.Length; i++)
            {
                   //if( rectangle_array[i].Area() < minPerimeter)   
                   if (rectangle_array[i] != null && rectangle_array[i].Area() < minPerimeter)
                {
                    minPerimeter = rectangle_array[i].Area();
                    numberOfRectangle = i;
                }
            }
            return numberOfRectangle;
        }

       
        public int NumberSquare()
        {
            int count = 0;
            for (int i = 0; i < rectangle_array.Length; i++)
            {
                //if (rectangle_array[i].IsSquare())  
                if (rectangle_array[i] != null && rectangle_array[i].IsSquare())
                {
                    count++;
                }
            }
            return count;
        }
    }
}
